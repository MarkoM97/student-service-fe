import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {

    constructor(private datePipe: DatePipe){}

  transform(value: Date): string {
      return this.datePipe.transform(value, "yyyy-MM-dd");
  }
}
