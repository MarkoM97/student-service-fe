import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StudentsComponent } from './components/students/students.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { StudentsAddComponent } from './components/students/students-add/students-add.component';
import { AuthenticationService } from './services/authentication-service.service';
import { JwtUtilsService } from './services/jwt-utils-service.service';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { TeacherComponent } from './components/teachers/teacher.component';
import { TeachersAddComponent } from './components/teachers/teachers-add/teachers-add.component';
import { TeacherDetailsComponent } from './components/teachers/teacher-details/teacher-details.component';
import { UserDetailsComponent } from './components/users/user-details/user-details.component';
import { StudentDetailsComponent } from './components/students/student-details/student-details.component';
import { UsersAddComponent } from './components/users/users-add/users-add.component';
import { RemainingSubjectsComponent } from './components/subjects/remaining-subjects/remaining-subjects.component';
import { HomeComponent } from './components/home/home.component';
import { ExaminationPeriodComponent } from './components/exams/examination-period/examination-period.component';
import { ExaminationPeriodAddComponent } from './components/exams/examination-period/examination-period-add/examination-period-add.component';
import { ExamComponent } from './components/exams/exam/exam.component';
import { ExamAddComponent } from './components/exams/exam/exam-add/exam-add.component';
import { StudentRegisteredExamsComponent } from './components/students/exams/registered-exams/student-registered-exams.component';
import { TeachersExamGradesComponent } from './components/teachers/exams/exam-grades/teachers-exam-grades.component';
import { StudentPassedExamsComponent } from './components/students/exams/passed-exams/student-passed-exams.component';
import { StudentExamRegistrationComponent } from './components/students/exams/exam-registration/student-exam-registration.component';
import { StudyProgramsComponent } from './components/study-programs/study-programs.component';
import { StudyProgramsAddComponent } from './components/study-programs/study-programs-add/study-programs-add.component';
import { CourseTypesComponent } from './components/course-types/course-types.component';
import { CourseTypesAddComponent } from './components/course-types/course-types-add/course-types-add.component';
import { TeacherCurrentExamsComponent } from './components/teachers/exams/teacher-current-exams/teacher-current-exams.component';
import { StudentPaymentsComponent } from './components/students/student-payments/student-payments.component';
import { AdminsComponent } from './components/admins/admins.component';
import { PaginationComponent } from './components/shared/pagination/pagination.component';
import { TeacherAllExamsComponent } from './components/teachers/exams/all-exams/teacher-all-exams.component';
import { AdminDetailsComponent } from './components/admins/admin-details/admin-details.component';
import { DatePipe } from '@angular/common';
import { StudentExamHistoryComponent } from './components/students/exams/exam-history/student-exam-history.component';
import { AddPaymentComponent } from './components/students/student-payments/add-payment/add-payment.component';
import { FormatDatePipe } from './pipe/format-date.pipe';
import { AdminEditComponent } from './components/admins/admin-edit/admin-edit.component';
import { DocumentsComponent } from './components/students/documents/documents.component';
import { DocumentsAddComponent } from './components/students/documents/documents-add/documents-add.component';
import { AdminAddComponent } from './components/admins/admin-add/admin-add.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    StudentsComponent,
    LoginComponent,
    StudentsAddComponent,
    TeacherComponent,
    TeachersAddComponent,
    TeacherDetailsComponent,
    UserDetailsComponent,
    StudentDetailsComponent,
    UsersAddComponent,
    RemainingSubjectsComponent,
    HomeComponent,
    ExaminationPeriodComponent,
    ExaminationPeriodAddComponent,
    ExamComponent,
    ExamAddComponent,
    StudentRegisteredExamsComponent,
    TeachersExamGradesComponent,
    StudentPassedExamsComponent,
    StudentExamRegistrationComponent,
    StudyProgramsComponent,
    StudyProgramsAddComponent,
    CourseTypesComponent,
    CourseTypesAddComponent,
    TeacherCurrentExamsComponent,
    StudentPaymentsComponent,
    AdminsComponent,
    PaginationComponent,
    TeacherAllExamsComponent,
    AdminDetailsComponent,
    StudentExamHistoryComponent,
    AddPaymentComponent,
    FormatDatePipe,
    AdminEditComponent,
    DocumentsComponent,
    DocumentsAddComponent
    AdminAddComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    AuthenticationService,
    DatePipe,
    JwtUtilsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
