import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import { AdminsComponent } from '../admins.component';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.css']
})
export class AdminAddComponent implements OnInit {
  firstName: string;
  lastName: string;
  address: string;
  birthday: Date;
  username: string;
  password: string;
  email: string;

  constructor(private userService: UsersService, private adminComponent: AdminsComponent) { }

  ngOnInit() {
  }

  toggleForm():void { this.adminComponent.toggleFalse(); }

  add():void {
    this.userService.addUser(this.firstName, this.lastName, this.address, this.birthday, this.username, this.password, this.email);
    this.toggleForm();
  }

}
