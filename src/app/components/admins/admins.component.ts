import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import User from 'src/app/model/User';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {
  //pagination related data
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  admins: User[];
  public isViewable: boolean;
  constructor(private userService: UsersService) { }

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  public paginationUpdate(number) {
    this.updateAdmins(number);
  } 

  ngOnInit() {
    // this.admins = [new User(1,'username','John','Snow',null,'12@123.com',null,null)]
    this.updateAdmins(1);
  }

  public updateAdmins(page, size = 5) {
    this.userService.getAdmins(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.admins = res.content;
    },err => {
      console.log(err);
    })
  }

  deleteAdmin(id){
    this.userService.deleteUser(id)
    .subscribe(res => {
      this.admins = this.admins.filter(admin => {
        return admin.id !== id;
      });
    }, err => {
        console.log(err);
    });
  }
}
