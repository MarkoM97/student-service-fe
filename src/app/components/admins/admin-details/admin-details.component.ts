import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import User from '../../../model/User';
import { AuthenticationService } from 'src/app/services/authentication-service.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-admin-details',
  templateUrl: './admin-details.component.html',
  styleUrls: ['./admin-details.component.css']
})
export class AdminDetailsComponent implements OnInit {

  admin: User
  public selectedAdmin: User;
  public isViewable: boolean;

  constructor(private usersService: UsersService,
    private authenticationService: AuthenticationService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.usersService.getUserById(token.id).subscribe(
        admin => {
          this.admin = admin;
          this.admin.birthday = this.datePipe.transform(this.admin.birthday, "yyyy-MM-dd");
        },
        error => console.log("Greska...")
      );
    }
  }

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  public edit(admin: User) {
    this.selectedAdmin = admin;
    this.toggle();
    console.log(admin);
  }

}
