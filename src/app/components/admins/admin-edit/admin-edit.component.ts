import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import User from 'src/app/model/User';
import { AdminDetailsComponent } from '../admin-details/admin-details.component';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.css']
})
export class AdminEditComponent implements OnInit {
  @Input() selectedAdmin: User;
  firstName: String;
  lastName: String;
  email: String;
  address: String;

  constructor(private adminEdit: AdminDetailsComponent, private userService: UsersService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) { 
    if (this.selectedAdmin !== undefined) {
      this.firstName = this.selectedAdmin.firstname;
      this.lastName = this.selectedAdmin.lastname;
      this.email = this.selectedAdmin.email;
      this.address = this.selectedAdmin.address;
    } else {
    }
  }

  toggleForm():void { 
    this.adminEdit.toggleFalse();
  }

  editAdmin():void {
    this.userService.updateAdmin(new User(this.selectedAdmin.id, this.selectedAdmin.username, this.selectedAdmin.password, this.firstName, 
      this.lastName, this.selectedAdmin.birthday, this.email, this.address, this.selectedAdmin.deleted)).subscribe(res => {
        
    }, err => {
        console.log(err);
    });
    this.adminEdit.toggleFalse();
  }

}
