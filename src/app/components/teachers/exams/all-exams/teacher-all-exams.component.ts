import { Component, OnInit } from '@angular/core';
import TeacherExam from '../../../../model/TeacherExam';
import { ExamsService } from 'src/app/services/exams/exams.service';
import { AuthenticationService } from '../../../../services/authentication-service.service';

@Component({
  selector: 'app-teacher-all-exams',
  templateUrl: './teacher-all-exams.component.html',
  styleUrls: ['./teacher-all-exams.component.css']
})
export class TeacherAllExamsComponent implements OnInit {

  exams: TeacherExam[];
  constructor(private authenticationService: AuthenticationService,
    private examsService: ExamsService ) {
  }

  ngOnInit(): void {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.examsService.getAllExamsForTeacher(token.id).subscribe(
        exams => {this.exams = exams; console.log(exams)},
        error => console.log("Greska...")
      );
    }
  }
}

