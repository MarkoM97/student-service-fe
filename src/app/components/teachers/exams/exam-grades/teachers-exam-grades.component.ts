import { Component, OnInit, Input } from '@angular/core';
import TeacherExamGrades from '../../../../model/TeacherExamGrades';
import { ExamGradesService } from 'src/app/services/exam-grades/exam-grades.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-teachers-exam-grades',
  templateUrl: './teachers-exam-grades.component.html',
  styleUrls: ['./teachers-exam-grades.component.css']
})
export class TeachersExamGradesComponent implements OnInit {
  @Input() examGrades: TeacherExamGrades[];
  subjectName: string;

  constructor(private route: ActivatedRoute,
    private examGradesService: ExamGradesService, 
    private router: Router) {}

  ngOnInit() {
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      this.examGradesService.getTeacherExamGrades(param).subscribe(
        examGrades =>{this.examGrades = examGrades; this.subjectName = this.examGrades[0].subjectName},
        error => console.log("Greska...")
      );
    }
  }
  saveGrades(){
    console.log(this.examGrades);
    this.examGradesService.createExamGrades(this.examGrades);
    this.router.navigate(['/teacher-exams']);
  }
}
