import { Component, OnInit } from '@angular/core';
import TeacherExam from '../../../../model/TeacherExam';
import { ExamsService } from 'src/app/services/exams/exams.service';
import { AuthenticationService } from '../../../../services/authentication-service.service';

@Component({
  selector: 'app-teacher-current-exams',
  templateUrl: './teacher-current-exams.component.html',
  styleUrls: ['./teacher-current-exams.component.css']
})
export class TeacherCurrentExamsComponent implements OnInit {

  exams: TeacherExam[];
  constructor(private authenticationService: AuthenticationService,
    private examsService: ExamsService ) {
  }

  ngOnInit(): void {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.examsService.getCurrentExamsForTeacher(token.id).subscribe(
        exams => {this.exams = exams; console.log(exams)},
        error => console.log("Greska...")
      );
    }
  }
  opetExamGrade(id: number){
    
  }
}