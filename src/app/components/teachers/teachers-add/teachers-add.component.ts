import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import Teacher from '../../../model/Teacher';
import TeacherType from '../../../model/TeacherType';
import { TeacherTypeService } from '../../../services/teacher-type/teacher-type.service';
import { TeachersService } from '../../../services/teachers/teachers.service';
import { TeacherComponent } from '../teacher.component';
import User from '../../../model/User';
import { UsersService } from '../../../services/users/users.service';
import TeacherDetails from 'src/app/model/TeacherDetails';
import TeacherDetails1 from 'src/app/model/TeacherDetails1';
import StudentDetails1 from 'src/app/model/StudentDetails1';

@Component({
  selector: 'app-teachers-add',
  templateUrl: './teachers-add.component.html',
  styleUrls: ['./teachers-add.component.css']
})
export class TeachersAddComponent implements OnInit {
  @Input() users: User[];
  @Input() teacherTypes: TeacherType[];
  @Input() selectedTeacher: TeacherDetails1;
  selectedTeacherType: string;
  selectedUser: number;
  errorMessage = '';
  isAddOperation: Boolean = true;

  firstName: String;
  lastName: String;
  email: String;
  address: String;
  teacherCode: number;

  constructor(private teacherTypeService: TeacherTypeService, private teacherService: TeachersService,
    private teacherComponent: TeacherComponent, private usersService: UsersService) { 
    this.teacherTypeService.getTeacherTypes()
      .subscribe(
      res => {
        this.teacherTypes = res;
      },
      error => {
        console.log("Greska...")
      });
      this.usersService.getUsersForTeacherCreate()
      .subscribe(
      res => {
        this.users = res;
      },
      error => {
        console.log("Greska...")
      });
  }

  ngOnInit(): void {
    /*this.teacherTypeService.getTeacherTypes().subscribe(
      teacherTypes => {
        this.teacherTypes = teacherTypes;
      },
      error => this.errorMessage = <any>error
    );*/
  }

  ngOnChanges(changes: SimpleChanges) { 
    if (this.selectedTeacher !== undefined) {
      this.firstName = this.selectedTeacher.firstname;
      this.lastName = this.selectedTeacher.lastname;
      this.email = this.selectedTeacher.email;
      this.address = this.selectedTeacher.address;
      this.teacherCode = this.selectedTeacher.teacherCode;

      this.selectedTeacherType = this.selectedTeacher.teacherType;
      this.isAddOperation = false;
    } else {
      this.isAddOperation = true;
    }
  }

  toggleForm():void { 
    this.teacherComponent.toggleFalse();
  }

  addTeacher():void{
    this.teacherService.addTeacher(this.selectedUser, this.selectedTeacherType);
    this.teacherComponent.toggleAddForm();
  }

  editTeacher():void {
    this.teacherComponent.updateTeacher(new TeacherDetails1(this.selectedTeacher.id, this.firstName, this.lastName, this.email, 
      this.address, this.selectedTeacherType, this.teacherCode));
    this.teacherComponent.toggleFalse();
  }

}
