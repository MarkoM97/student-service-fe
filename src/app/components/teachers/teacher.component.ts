import { Component, OnInit } from '@angular/core';
import { TeachersService } from 'src/app/services/teachers/teachers.service';
import TeacherDetails from 'src/app/model/TeacherDetails';
import TeacherDetails1 from 'src/app/model/TeacherDetails1';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  //pagination related data
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;
  
  teachers = [];
  showAddForm: boolean;
  errorMessage = '';

  public isViewable: boolean;
  public selectedTeacher: TeacherDetails1;

  constructor(private teacherService:TeachersService) {}

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  public paginationUpdate(number) {
    this.updateAdmins(number);
  } 

  public updateAdmins(page, size = 5) {
    this.teacherService.getTeachers(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.teachers = res.content;
    },err => {
      console.log(err);
    })
  }

  deleteTeacher(id: number): void{
    this.teacherService.deleteTeacher(id);
    document.getElementById("tr_"+ id).remove();

  }

  public toggleAddForm(): void { 
    this.showAddForm = !this.showAddForm; }

  ngOnInit() {
    this.updateAdmins(1);
  }

  public editTeacher(teacher: TeacherDetails1) {
    this.selectedTeacher = teacher;
    this.toggle();
  }

  updateTeacher(teacher: TeacherDetails1) {
    console.log(teacher);
    this.teacherService.updateTeacher(teacher)
      .subscribe(res => {
        this.toggleFalse();
        const sp = this.teachers.findIndex(stpg => {
          return teacher.id === stpg.id;
        });
        this.teachers[sp] = teacher;
    }, err => {
        console.log(err);
    });
  }

}
