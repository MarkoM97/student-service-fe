import { Component, OnInit } from '@angular/core';
import { TeachersService } from '../../../services/teachers/teachers.service';
import TeacherDetails from '../../../model/TeacherDetails';
import { AuthenticationService } from '../../../services/authentication-service.service';

@Component({
  selector: 'app-teacher-details',
  templateUrl: './teacher-details.component.html',
  styleUrls: ['./teacher-details.component.css']
})
export class TeacherDetailsComponent implements OnInit {
  teacher: TeacherDetails;
  public selectedTeacher: TeacherDetails;
  public isViewable: boolean;

  constructor(private authenticationService: AuthenticationService,
    private teachersService: TeachersService ) {
    }

  ngOnInit(): void {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.teachersService.getTeacher(token.id).subscribe(
        teacher => this.teacher = teacher,
        error => console.log("Greska...")
      );
    }
  }

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  public edit(teacher: TeacherDetails) {
    this.selectedTeacher = teacher;
    this.toggle();
  }

}
