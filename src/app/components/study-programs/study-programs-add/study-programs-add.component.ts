import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import StudyProgram from 'src/app/model/StudyProgram';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import { StudyProgramsComponent } from '../study-programs.component';
import CourseType from 'src/app/model/CourseType';
import { CourseTypesService } from 'src/app/services/course-types/course-types.service';


@Component({
  selector: 'app-study-programs-add',
  templateUrl: './study-programs-add.component.html',
  styleUrls: ['./study-programs-add.component.css']
})
export class StudyProgramsAddComponent implements OnInit {
  @Input() selectedStudyProgram: StudyProgram;

  courseTypes: CourseType[];
  name: string;
  duration: Number;
  courseType: Number;
  selectedCourseType: String;

  isAddOperation: Boolean = true;

  constructor(private courseTypeService: CourseTypesService, private studentComponent: StudyProgramsComponent) {}

  ngOnInit() {
    this.courseTypeService.getCourseTypes(1, 100) // xD x2
    .subscribe(
      res => {
        this.courseTypes = res.content;
      },
      error => {
        console.log("Greska...")
      });
  }


  ngOnChanges(changes: SimpleChanges) {
    if(this.selectedStudyProgram !== undefined) {
      this.name = this.selectedStudyProgram.name;
      this.duration = this.selectedStudyProgram.duration;
      this.selectedCourseType = this.selectedStudyProgram.courseType.name;
      
      //ui
      this.isAddOperation = false;
    } else {
      this.courseTypeService.getCourseTypes(1, 100) // xD x2
      .subscribe(
        res => {
          this.courseTypes = res.content;
          this.name = "";
          this.duration = 0;          
          this.selectedCourseType = this.courseTypes[0].name;
          this.isAddOperation = true;
        },
        error => {
          console.log("Greska...")
        });
    }
  }

  toggleForm():void { this.studentComponent.toggleFalse(); }

  getCourseTypeByName(name: String) {
      return this.courseTypes.filter(x => {
          console.log(name);
          return x.name === name;
      })[0];
  }

  addStudent():void {
    console.log(this.selectedStudyProgram);
    if(!this.isAddOperation) {
      this.studentComponent.updateStudyProgram(new StudyProgram(this.selectedStudyProgram.id, this.name, this.duration, this.getCourseTypeByName(this.selectedCourseType)));
    } else {
      this.studentComponent.addStudyProgram(new StudyProgram(null, this.name, this.duration, this.getCourseTypeByName(this.selectedCourseType)));
    }
  }
}
