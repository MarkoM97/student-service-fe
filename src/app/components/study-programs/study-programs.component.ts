import { Component, OnInit, NgZone } from '@angular/core';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import { ActivatedRoute } from '@angular/router';
import StudyProgram from 'src/app/model/StudyProgram';
import PaginationInfo from 'src/app/model/Pagination';

@Component({
  selector: 'app-study-programs',
  templateUrl: './study-programs.component.html',
  styleUrls: ['./study-programs.component.css']
})
export class StudyProgramsComponent implements OnInit {
  public studyPrograms = [];
  public paginationInfo: PaginationInfo;
  public selectedStudyProgram: StudyProgram;

  public isViewable: boolean;

  constructor(private route: ActivatedRoute, private studyProgramService: StudyProgramService) {}

  ngOnInit() {
    this.listStudyPrograms(1);
  }

  public update(studyProgram: StudyProgram){
      this.selectedStudyProgram = studyProgram;
      this.isViewable = true;
  }

  public toggle(): void {
    if(this.isViewable) {
      this.selectedStudyProgram = undefined;
    }  else {
      this.isViewable = true;
    }
  }

  public toggleFalse(): void { 
    this.isViewable = false;
    this.selectedStudyProgram = undefined;
   }

  //pagination
  public paginationUpdate(msg) {
    this.listStudyPrograms(msg.page, msg.size);
  } 

  listStudyPrograms(page, size = 2) {
    this.studyProgramService.getStudyPrograms(page, size).subscribe(res => {
      this.paginationInfo = res;
      this.studyPrograms = res.content;
    },err => {
      console.log(err);
    })
  }


  addStudyProgram(studyProgram: StudyProgram) {
    this.studyProgramService.addStudyProgram(studyProgram)
      .subscribe(res => {
          this.toggleFalse();    
      }, err => {
          console.log(err);
      });
  }

  deleteStudyProgram(id: Number) {
    this.studyProgramService.deleteStudyProgram(id)
    .subscribe(res => {
      console.log(res);
      this.studyPrograms = this.studyPrograms.filter(studyProgram => {
        return studyProgram.id !== id;
      });
  }, err => {
      console.log(err);
  });
  }

  updateStudyProgram(studyProgram: StudyProgram) {
    this.studyProgramService.updateStudyProgram(studyProgram)
      .subscribe(res => {
        this.toggleFalse();
        const sp = this.studyPrograms.findIndex(stpg => {
          return studyProgram.id === stpg.id;
        });
        this.studyPrograms[sp] = studyProgram;
    }, err => {
        console.log(err);
    });
  }
}
