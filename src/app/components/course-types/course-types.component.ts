import { Component, OnInit } from '@angular/core';
import { CourseTypesService } from 'src/app/services/course-types/course-types.service';
import CourseType from 'src/app/model/CourseType';

@Component({
  selector: 'app-course-types',
  templateUrl: './course-types.component.html',
  styleUrls: ['./course-types.component.css']
})
export class CourseTypesComponent implements OnInit {
  //pagination related data
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  courseTypes = [];
  public selectedCourseType: CourseType;
  public isViewable: boolean;

  constructor(private courseTypesService: CourseTypesService) { }

  ngOnInit() {
    this.updateCourseTypes(1);
    this.isViewable = false;
  }

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  public updateCourseTypes(page, size = 2) {
    this.courseTypesService.getCourseTypes(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.courseTypes = res.content;
    },err => {
      console.log(err);
    })
  }

  public paginationUpdate(number) {
    this.updateCourseTypes(number);
  }
  
  public deleteCourseType(id: Number){
    this.courseTypesService.deleteCourseType(id)
    .subscribe(res => {
      console.log(res);
      this.courseTypes = this.courseTypes.filter(courseType => {
        return courseType.id !== id;
      });
    }, err => {
        console.log(err);
    });
  }

  public editCourseType(courseType: CourseType) {
    this.selectedCourseType = courseType;
    this.toggle();
  }

  updateCourseType(courseType: CourseType) {
    this.courseTypesService.updateCourseType(courseType)
      .subscribe(res => {
        this.toggleFalse();
        const sp = this.courseTypes.findIndex(stpg => {
          return courseType.id === stpg.id;
        });
        this.courseTypes[sp] = courseType;
    }, err => {
        console.log(err);
    });
  }


}
