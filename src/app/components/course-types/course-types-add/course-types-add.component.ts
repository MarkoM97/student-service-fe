import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { CourseTypesComponent } from '../course-types.component';
import { CourseTypesService } from 'src/app/services/course-types/course-types.service';
import CourseType from 'src/app/model/CourseType';

@Component({
  selector: 'app-course-types-add',
  templateUrl: './course-types-add.component.html',
  styleUrls: ['./course-types-add.component.css']
})
export class CourseTypesAddComponent implements OnInit {
  @Input() selectedCourseType: CourseType;

  name: String;

  isAddOperation: Boolean = true;

  constructor(private courseTypeComponent: CourseTypesComponent, private courseTypeService: CourseTypesService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) { 
    if (this.selectedCourseType !== undefined) {
      this.name = this.selectedCourseType.name;
      this.isAddOperation = false;
    } else {
      this.isAddOperation = true;
    }
  }

  toggleForm():void {
    this.courseTypeComponent.toggleFalse();
  }

  addCourseType() {
    this.courseTypeService.addCourseType(this.name);
    this.toggleForm();
  }

  add():void {
    if(!this.isAddOperation) {
      this.courseTypeComponent.updateCourseType(new CourseType(this.selectedCourseType.id, this.name));
    } else {
      this.courseTypeService.addCourseType(this.name);
    }
  }

}
