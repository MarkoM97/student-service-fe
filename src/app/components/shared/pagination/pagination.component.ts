import { Component, OnInit, NgZone, Output, Input } from '@angular/core';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import { ActivatedRoute } from '@angular/router';
import StudyProgram from 'src/app/model/StudyProgram';
import { EventEmitter } from '@angular/core';
import PaginationInfo from 'src/app/model/Pagination';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
})
export class PaginationComponent implements OnInit {
  @Input() paginationInfo: PaginationInfo;
  @Output() paginationUpdate = new EventEmitter<any>();
  
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;


  constructor() {}

  public update(number) {
    this.updateItems(number);
  } 

  public updateItems(page, size = 2) {
    this.paginationUpdate.emit({page: page, size: 2});
  }

  ngOnInit() {}

  ngOnChanges() {
    if(typeof this.paginationInfo === "object") {
        this.totalPageNumber = Array(this.paginationInfo.totalPages).fill(this.paginationInfo.totalPages).map((x,i) => i + 1);
        this.page = this.paginationInfo.number + 1;
        this.isFirst = this.paginationInfo.first;
        this.isLast = this.paginationInfo.last;
    }
  }
}
