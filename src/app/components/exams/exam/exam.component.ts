import { Component, OnInit } from '@angular/core';
import { ExamService } from 'src/app/services/exam/exam.service';
import Exam from 'src/app/model/Exam';
import Subject from 'src/app/model/Subject';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  //pagination related data
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  exams = [];
  public selectedExam: Exam;
  public isViewable: boolean;

  constructor(private examService: ExamService) { }

  public toggle(): void { this.isViewable = !this.isViewable; }

  public toggleFalse(): void { this.isViewable = false; }

  public getExams(page, size = 2) {
    this.examService.getDetailedExams(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.exams = res.content;
    },err => {
      console.log(err);
    })
  }

  ngOnInit() {
    this.getExams(1);
  }

  public paginationUpdate(number) {
    console.log(number);
    this.getExams(number);
  } 

  deleteExam(id: Number): void{
    this.examService.delete(id)
    .subscribe(res => {
      this.exams = this.exams.filter(ep => {
        return ep.id !== id;
      });
    }, err => {
        console.log(err);
    });
  }

  public updateSelectedE(exam: Exam){
    this.selectedExam = exam;
    this.isViewable = true;
  }

  updateExam(exam: Exam) {
    this.examService.update(exam)
      .subscribe(res => {
        this.toggle();
        const ep = this.exams.findIndex(item => {
          return exam.id === item.id;
        });
        this.exams[ep] = exam;
    }, err => {
        console.log(err);
    });
  }
}
