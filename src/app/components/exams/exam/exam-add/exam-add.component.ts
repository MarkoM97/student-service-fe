import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import Exam from 'src/app/model/Exam';
import { ExamService } from 'src/app/services/exam/exam.service';
import { SubjectsService } from 'src/app/services/subjects/subjects.service';
import Subject from 'src/app/model/Subject';
import { ExamComponent } from '../exam.component';

@Component({
  selector: 'app-exam-add',
  templateUrl: './exam-add.component.html',
  styleUrls: ['./exam-add.component.css']
})
export class ExamAddComponent implements OnInit {
  @Input() subjects: Subject[];
  @Input() selectedExam: Exam;

  date: Date;
  time: String;
  classRoom: String;
  selectedSubject: Number;
  buttonText: String = 'Add'

  constructor(private examService: ExamService, private subjectService: SubjectsService, private examComponent: ExamComponent) { 

  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getSubjects();
  }

  toggleForm():void { this.examComponent.toggleFalse(); }
  
  getSubjects(){
    this.subjectService.getAllCourses()
    .subscribe(
      res => {
        console.log(res);
        this.subjects = res;
        console.log(res);
        this.prepareFields();
      },
      error => {
        console.log("Greska...")
      });
  }

  prepareFields(){
    console.log(this.selectedExam.subject);
    if(this.selectedExam !== undefined) {
      this.date = this.selectedExam.date;
      this.time = this.selectedExam.time;
      this.classRoom = this.selectedExam.classRoom;
      this.selectedSubject = this.selectedExam.subject.id;
      this.buttonText = "Update";
    } else {
      this.date = new Date();
      this.time = '';
      this.classRoom = '';
      this.selectedSubject = undefined;
    }
  }

  addExam(){
    console.log(this.selectedExam);
    const subject = new Subject(this.selectedSubject,null,null);
    if(this.buttonText === "Update") {
      this.examComponent.updateExam(new Exam(this.selectedExam.id, this.date, this.time, this.classRoom, subject));
    } else {
      this.examService.addExam(new Exam(null, this.date, this.time, this.classRoom, subject))
      .subscribe(res => {
          this.examComponent.toggle();    
      }, err => {
          console.log(err);
      });
    }
  }
}
