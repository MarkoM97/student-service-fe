import { Component, OnInit } from '@angular/core';
import { ExaminationPeriodService } from 'src/app/services/examination-period/examination-period.service';
import ExaminationPeriod from 'src/app/model/ExaminationPeriod';

@Component({
  selector: 'app-examination-period',
  templateUrl: './examination-period.component.html',
  styleUrls: ['./examination-period.component.css']
})
export class ExaminationPeriodComponent implements OnInit {
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  examPeriods = [];
  public isViewable: boolean;
  public selectedExamPeriod: ExaminationPeriod;
  constructor(private examPeriodService: ExaminationPeriodService) { }

  ngOnInit() {
    // this.examPeriods = [
    //   {
    //     id:1,
    //     name:'OSA',
    //     startDate:new Date("January 1 1980"),
    //     endDate:new Date("January 31 1980"),
    //     deleted:false,
    //   }
    // ]
    this.getAllExamPeriods(1);
  }

  getAllExamPeriods(page,size=2){
    this.examPeriodService.getExamPeriods(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.examPeriods = res.content;
    },err => {
      console.log(err);
    })
  }

  public toggle(): void { 
    if(this.isViewable) {
      this.selectedExamPeriod = undefined;
    }
    this.isViewable = !this.isViewable;
  }

  public paginationUpdate(number) {
    this.getAllExamPeriods(number);
  } 

  deleteExamPeriod(id: Number): void{
    this.examPeriodService.delete(id)
    .subscribe(res => {
      this.examPeriods = this.examPeriods.filter(ep => {
        return ep.id !== id;
      });
    }, err => {
        console.log(err);
    });
  }

  public updateSelectedEP(examPeriod: ExaminationPeriod){
    this.selectedExamPeriod = examPeriod;
    this.isViewable = true;
  }

  updateExaminationPeriod(examPeriod: ExaminationPeriod) {
    this.examPeriodService.update(examPeriod)
      .subscribe(res => {
        this.toggle();
        const ep = this.examPeriods.findIndex(item => {
          return examPeriod.id === item.id;
        });
        this.examPeriods[ep] = examPeriod;
    }, err => {
        console.log(err);
    });
  }
}
