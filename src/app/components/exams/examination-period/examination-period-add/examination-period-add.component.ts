import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import ExaminationPeriod from 'src/app/model/ExaminationPeriod';
import Exam from 'src/app/model/Exam';
import { ExaminationPeriodService } from 'src/app/services/examination-period/examination-period.service';
import { ExaminationPeriodComponent } from '../examination-period.component';
import { ExamsService } from 'src/app/services/exams/exams.service';
import { ExamService } from 'src/app/services/exam/exam.service';
import Subject from 'src/app/model/Subject';

@Component({
  selector: 'examination-period-add',
  templateUrl: './examination-period-add.component.html',
  styleUrls: ['./examination-period-add.component.css']
})
export class ExaminationPeriodAddComponent implements OnInit {
  @Input() selectedExamPeriod: ExaminationPeriod;
  @Input() exams: Exam[];

  name: String;
  startDate: Date;
  endDate: Date;
  selectedExam: Number;
  selectedExams: Number[];
  buttonText: String = 'Add';

  constructor(private examPeriodService: ExaminationPeriodService,
              private examPeriodComponent: ExaminationPeriodComponent,
              private examService: ExamService) { 
                
              }

  toggleForm():void { this.examPeriodComponent.toggle(); }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getExams();
  }

  getExams(){
    this.examService.getAllDetailedExams().subscribe(res => {
      this.exams = res;
      console.log(res);
      this.prepareFields();
    },err => {
      console.log(err);
    })
  }
  prepareFields(){
    console.log(this.selectedExamPeriod.exams);
    if(this.selectedExamPeriod !== undefined) {
      this.name = this.selectedExamPeriod.name;
      this.startDate = this.selectedExamPeriod.startDate;
      this.endDate = this.selectedExamPeriod.endDate;
      this.selectedExams = this.selectedExamPeriod.exams;
      this.buttonText = "Update";
    } else {
      this.name = '';
      this.startDate = new Date();
      this.endDate = new Date();
      this.selectedExams = [];
    }
  }
  
  addExamPeriod(){
    if(this.buttonText === "Update") {
      this.examPeriodComponent.updateExaminationPeriod(new ExaminationPeriod(this.selectedExamPeriod.id, this.name, this.startDate, this.endDate, this.selectedExams));
    } else {
      this.examPeriodService.addExamPeriod(new ExaminationPeriod(null, this.name, this.startDate, this.endDate, this.selectedExams))
      .subscribe(res => {
          this.examPeriodComponent.toggle();    
      }, err => {
          console.log(err);
      });
    }
  }

  
}