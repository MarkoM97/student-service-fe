import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication-service.service';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  public user;
  public wrongUsernameOrPass:boolean;

  constructor(private authenticationService:AuthenticationService, private router:Router) {
    this.user = {};
    this.wrongUsernameOrPass = false;
   }

  ngOnInit() {
  }

  login():void{
    this.authenticationService.login(this.user.username, this.user.password).subscribe(
      (loggedIn:boolean) => {
        if(loggedIn){
          const userRole = this.authenticationService.getTokenInfo().role
          if(userRole.includes("ADMINISTRATOR")) {
            this.router.navigate(['/']); 
          } else if(userRole.includes("TEACHER")) {
            this.router.navigate(['/teacher-profile']);
          } else if(userRole.includes("STUDENT")) {
            this.router.navigate(['/student-profile']);
          }
        }
      }
    ,
    (err:Error) => {
      if(err.toString()==='Ilegal login'){
        this.wrongUsernameOrPass = true;
        console.log(err);
      }
      else{
        throwError(err);
      }
    });
  }

}
