import { Component, OnInit} from '@angular/core';
import StudentPayment from '../../../../model/StudentPayment';
import { StudentPaymentsComponent } from '../student-payments.component';
import { StudentPaymentsService } from '../../../../services/student-payments/student-payments.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  styleUrls: ['./add-payment.component.css']
})
export class AddPaymentComponent implements OnInit {
  payment: StudentPayment;

  description: string;
  amount: number;

  constructor(private studentPaymentsService: StudentPaymentsService,
    private studentPayment: StudentPaymentsComponent,
    private router: Router) { }

  ngOnInit() {
  }

  addPayment():void{
    var date = new Date().toJSON().slice(0,10).replace(/-/g,'/');

    this.payment = new StudentPayment(null, this.description, new Date(), this.amount, this.studentPayment.studentId);
    this.studentPaymentsService.addPayment(this.payment);
    alert("Successfuly added payment");
    this.studentPayment.toggleAddForm();
    this.studentPayment.studentPayments.push(this.payment);

  }

  toggleForm(){
    this.studentPayment.toggleAddForm();
  }

}
