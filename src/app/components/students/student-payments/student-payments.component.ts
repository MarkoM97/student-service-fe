import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudentPaymentsService } from 'src/app/services/student-payments/student-payments.service';
import StudentPayment from '../../../model/StudentPayment';

@Component({
  selector: 'app-student-payments',
  templateUrl: './student-payments.component.html',
  styleUrls: ['./student-payments.component.css']
})
export class StudentPaymentsComponent implements OnInit {
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  studentPayments: StudentPayment[]
  studentId: String;  
  showAddForm: boolean;


  constructor(private route: ActivatedRoute,
    private studentPaymentsService: StudentPaymentsService) { }

  ngOnInit() {
    this.studentId = this.route.snapshot.paramMap.get('id');
    this.getStudentPayments(1);
  }

  public paginationUpdate(number) {
    this.getStudentPayments(number);
  } 

  public toggleAddForm(): void { 
    this.showAddForm = !this.showAddForm; }


  getStudentPayments(page, size = 2){
    // this.studentPayments= [new StudentPayment(1,'First scollarship payment', new Date().toDateString(), 2000)]
    if (this.studentId) {
      this.studentPaymentsService.getUserPayments(this.studentId, page, size).subscribe(
        res => {this.studentPayments = res
        
        //pagination related data
        this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
        this.page = res.number + 1;
        this.isFirst = res.first;
        this.isLast = res.last;

        this.studentPayments = res.content;
      },err => {
        console.log(err);
      });
    }
  }
}
