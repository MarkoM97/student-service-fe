import { Component, OnInit } from '@angular/core';
import { StudentsService } from 'src/app/services/students/students.service';
import { ActivatedRoute } from '@angular/router';
import UserStudent from 'src/app/model/UserStudent';
import StudentDetails from 'src/app/model/StudentDetails';
import StudentDetails1 from 'src/app/model/StudentDetails1';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  //pagination related data
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  students = [];
  public isViewable: boolean;
  public selectedStudent: StudentDetails;

  constructor(private route: ActivatedRoute, private studentService:StudentsService) {}


  deleteStudent(id: number): void {
    this.studentService.deleteStudent(id);
    document.getElementById("tr_"+ id).remove();
  }

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  public paginationUpdate(number) {
    this.updateStudents(number);
  } 

  public updateStudents(page, size = 5) {
    this.studentService.getStudents(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.students = res.content;
      console.log(res.content);
    },err => {
      console.log(err);
    })
  }

  ngOnInit() {
    this.isViewable = false;
    this.updateStudents(1);
  }

  public editStudent(student: StudentDetails) {
    this.selectedStudent = student;
    this.toggle();
  }

  updateStudent(student: StudentDetails1) {
    console.log(student);
    this.studentService.updateUser(student)
      .subscribe(res => {
        this.toggleFalse();
        const sp = this.students.findIndex(stpg => {
          return student.id === stpg.id;
        });
        this.students[sp] = student;
    }, err => {
        console.log(err);
    });
  }
  addStudent(student: StudentDetails1) {
    this.studentService.addStudent(student)
      .subscribe(res => {
        this.toggleFalse();
        const sp = this.students.findIndex(stpg => {
          return student.id === stpg.id;
        });
        this.students[sp] = student;
    }, err => {
        console.log(err);
    });
  }

}
