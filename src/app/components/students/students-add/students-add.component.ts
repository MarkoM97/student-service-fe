import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import Student from 'src/app/model/Student';
import StudyProgram from 'src/app/model/StudyProgram';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import { StudentsService } from 'src/app/services/students/students.service';
import { StudentsComponent } from '../students.component';
import UserStudent from 'src/app/model/UserStudent';
import StudentDetails from 'src/app/model/StudentDetails';
import StudentDetails1 from 'src/app/model/StudentDetails1';
import { AuthenticationService } from '../../../services/authentication-service.service';


@Component({
  selector: 'app-students-add',
  templateUrl: './students-add.component.html',
  styleUrls: ['./students-add.component.css']
})
export class StudentsAddComponent implements OnInit {
  @Input() student: Student;
  @Input() studyPrograms : StudyProgram[];
  @Input() selectedStudent: StudentDetails1;

  username: string;
  password: string;
  firstName: String;
  lastName: String;
  email: String;
  address: String;
  birthday: Date;

  cardNumber: String;
  accountNumber: String;
  balance: Number;
  selectedStudyProgram: string;
  modelNumber: String;
  isAddOperation: Boolean = true;
  isAdmin: Boolean = false;

  constructor(private authenticationService: AuthenticationService,
    private studyProgramService: StudyProgramService, private studentsService: StudentsService, private studentComponent: StudentsComponent) {
    this.studyProgramService.getStudyPrograms(1, 100) // xD hehe
    .subscribe(
      res => {
        this.studyPrograms = res.content;
      },
      error => {
        console.log("Greska...")
      });
  }

  ngOnInit() {
    const userRole = this.authenticationService.getTokenInfo().role
    if(userRole.includes("ADMINISTRATOR")) {
      this.isAdmin = true;
    }
  }

  ngOnChanges(changes: SimpleChanges) { 
    if (this.selectedStudent !== undefined) {
      this.firstName = this.selectedStudent.firstname;
      this.lastName = this.selectedStudent.lastname;
      this.email = this.selectedStudent.email;
      this.address = this.selectedStudent.address;

      this.cardNumber = this.selectedStudent.cardNumber;
      this.accountNumber = this.selectedStudent.accountNumber;
      this.balance = this.selectedStudent.balance;
      this.modelNumber = this.selectedStudent.modelNumber;
      this.selectedStudyProgram = this.selectedStudent.courseAttendingDTO.studyProgramName;
      this.isAddOperation = false;
    } else {
      this.isAddOperation = true;
    }
  }

  toggleForm():void { 
    this.studentComponent.toggleFalse();
  }

  getStudyByNesto(name: String) {
    return this.studyPrograms.filter(x => {
        console.log(name);
        return x.name === name;
    })[0];
}

  editStudent():void {
    if(this.isAddOperation == true){
      this.studentComponent.addStudent(new StudentDetails1(null, this.username, this.password, this.birthday, this.firstName, this.lastName, this.email, this.address, 
        this.cardNumber, this.accountNumber, this.modelNumber, this.balance, this.selectedStudyProgram, this.selectedStudyProgram));
      this.studentComponent.toggleFalse();
    }
    else{
      this.studentComponent.updateStudent(new StudentDetails1(this.selectedStudent.id, this.username, this.password, this.birthday, this.firstName, this.lastName, this.email, this.address, 
        this.cardNumber, this.accountNumber, this.modelNumber, this.balance, this.selectedStudyProgram, this.selectedStudyProgram));
      this.studentComponent.toggleFalse();
    }
  }

}
