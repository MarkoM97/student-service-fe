import { Component, OnInit } from '@angular/core';
import ExamRegistration from '../../../../model/ExamRegistration';
import { ExamsService } from '../../../../services/exams/exams.service';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../../services/authentication-service.service';

@Component({
  selector: 'app-student-registered-exams',
  templateUrl: './student-registered-exams.component.html',
  styleUrls: ['./student-registered-exams.component.css']
})
export class StudentRegisteredExamsComponent implements OnInit {

  registeredExams: ExamRegistration;

  constructor(private authenticationService: AuthenticationService,
    private examsService: ExamsService) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.examsService.getRegisteredExams(token.id).subscribe(
        exams => this.registeredExams = exams,
        error => console.log("Greska...")
      );
    }
  }
}
