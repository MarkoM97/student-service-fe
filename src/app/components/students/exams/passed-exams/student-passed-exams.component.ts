import { Component, OnInit } from '@angular/core';
import { ExamsService } from 'src/app/services/exams/exams.service';
import ExamPassed from '../../../../model/ExamPassed';
import { AuthenticationService } from '../../../../services/authentication-service.service';

@Component({
  selector: 'app-student-passed-exams',
  templateUrl: './student-passed-exams.component.html',
  styleUrls: ['./student-passed-exams.component.css']
})
export class StudentPassedExamsComponent implements OnInit {
  exams: ExamPassed[];
  averageGrade: number = 0;

  constructor(private authenticationService: AuthenticationService,
    private examsService: ExamsService) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.examsService.getExamPassedByStudent(token.id).subscribe(
        exams => {
          this.exams = exams;
          this.exams.forEach(exam => {
            this.averageGrade = this.averageGrade + exam.grade;
          });
          this.averageGrade = +(this.averageGrade / this.exams.length).toFixed(2);
        },
        error => console.log("Greska...")
      );
    }
  }

}
