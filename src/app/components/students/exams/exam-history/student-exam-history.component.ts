import { Component, OnInit } from '@angular/core';
import ExamPassed from '../../../../model/ExamPassed';
import { AuthenticationService } from '../../../../services/authentication-service.service';
import { ExamsService } from '../../../../services/exams/exams.service';

@Component({
  selector: 'app-student-exam-history',
  templateUrl: './student-exam-history.component.html',
  styleUrls: ['./student-exam-history.component.css']
})
export class StudentExamHistoryComponent implements OnInit {

  exams: ExamPassed[];

  constructor(private authenticationService: AuthenticationService,
    private examsService: ExamsService) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.examsService.getAllExamsForStudent(token.id).subscribe(
        exams => this.exams = exams,
        error => console.log("Greska...")
      );
    }
  }

}