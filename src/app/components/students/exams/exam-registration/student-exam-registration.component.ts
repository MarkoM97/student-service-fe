import { Component, OnInit } from '@angular/core';
import ExamRegistration from '../../../../model/ExamRegistration';
import { ExamsService } from '../../../../services/exams/exams.service';
import { AuthenticationService } from '../../../../services/authentication-service.service';

@Component({
  selector: 'app-student-exam-registration',
  templateUrl: './student-exam-registration.component.html',
  styleUrls: ['./student-exam-registration.component.css']
})
export class StudentExamRegistrationComponent implements OnInit {

  examRegistrations: ExamRegistration[];
  studentBalance: number;
  studentId: number;
  errorMessage: string;

  constructor(private authenticationService: AuthenticationService,
    private examsService: ExamsService) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.studentId = token.id;
      this.examsService.getCurrentPeriod(this.studentId).subscribe(
        exams => {
          console.log(exams);
          this.examRegistrations = exams; 
          this.studentBalance = exams[0].studentBalance},
        error => console.log("Greska...")
      );
    }
  }

  registerExam(examId: number, ): void{
    if((this.studentBalance - 200) >= 0){
      this.examsService.registerExam(examId, this.studentId).subscribe(
      error => this.errorMessage = error
      );
    
      document.getElementById("btn"+ examId).remove();
      if(this.errorMessage == null){
        document.getElementById("td"+ examId).append("Successfully registered exam");
      }else{
        document.getElementById("td"+ examId).append(this.errorMessage);
      }
      this.studentBalance = this.studentBalance - 200;
    }else{
      alert("You don't have enought mony on your account");
    }
    
  }

}
