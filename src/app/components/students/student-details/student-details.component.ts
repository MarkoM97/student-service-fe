import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../../../services/students/students.service';
import StudentDetails from '../../../model/StudentDetails';
import { AuthenticationService } from '../../../services/authentication-service.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  student: StudentDetails;

  constructor(private authenticationService: AuthenticationService,
    private studentsService:StudentsService) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.studentsService.getStudentById(token.id).subscribe(
        student => {
          this.student = student
        },
        error => console.log("Greska...")
      );
    }
  }
}
