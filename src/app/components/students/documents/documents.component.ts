import { Component, OnInit, NgZone } from '@angular/core';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import { ActivatedRoute } from '@angular/router';
import StudyProgram from 'src/app/model/StudyProgram';
import PaginationInfo from 'src/app/model/Pagination';
import { DocumentService } from 'src/app/services/document/document.service';
import { AuthenticationService } from 'src/app/services/authentication-service.service';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  public documents = [];
  public paginationInfo: PaginationInfo;

  public isViewable: boolean;

  constructor(private route: ActivatedRoute, private documentService: DocumentService, private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.listDocuments(1);
  }

  public update(studyProgram: StudyProgram){
      this.isViewable = true;
  }

  public toggle(): void {
    if(this.isViewable) {
      //this.selectedStudyProgram = undefined;
    }  else {
      this.isViewable = true;
    }
  }

  public toggleFalse(): void { 
    this.isViewable = false;
   }

  //pagination
  public paginationUpdate(msg) {
    this.listDocuments(msg.page, msg.size);
  } 

  listDocuments(page, size = 2) {
    this.documentService.getDocuments(this.authenticationService.getTokenInfo().id).subscribe(res => {
      this.paginationInfo = res;
      this.documents = res.content;
    },err => {
      console.log(err);
    })
  }


  public deleteDocument(id: Number) {
    this.documentService.deleteDocument(id).subscribe(res => {
      console.log(res);
    })
  }

  public downloadDocument(id: Number) {
    window.location.href = `http://localhost:8080/api/documents/download/${id}`;

    // this.documentService.startDownload(id)
    // .subscribe(data => {
    //   const blob = new Blob([data], { type: 'text/csv' });
    //   const url= window.URL.createObjectURL(blob);
    //   window.open(url);
    //   })
  }

  // addDocument(studyProgram: StudyProgram) {
  //   this.documentService.addDocument(studyProgram)
  //     .subscribe(res => {
  //         this.toggleFalse();    
  //     }, err => {
  //         console.log(err);
  //     });
  // }

  // deleteDocument(id: Number) {
  //   this.studyProgramService.deleteStudyProgram(id)
  //   .subscribe(res => {
  //     console.log(res);
  //     this.studyPrograms = this.studyPrograms.filter(studyProgram => {
  //       return studyProgram.id !== id;
  //     });
  // }, err => {
  //     console.log(err);
  // });
  // }
}
