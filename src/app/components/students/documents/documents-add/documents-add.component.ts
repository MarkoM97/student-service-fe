import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import StudyProgram from 'src/app/model/StudyProgram';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import CourseType from 'src/app/model/CourseType';
import { CourseTypesService } from 'src/app/services/course-types/course-types.service';
import { DocumentsComponent } from '../documents.component';
import { DocumentService } from 'src/app/services/document/document.service';
import { AuthenticationService } from 'src/app/services/authentication-service.service';


@Component({
  selector: 'app-documents-add',
  templateUrl: './documents-add.component.html',
  styleUrls: ['./documents-add.component.css']
})
export class DocumentsAddComponent implements OnInit {

  name: string;
  file: File = null;
  constructor(private courseTypeService: CourseTypesService, private documentsComponent: DocumentsComponent, private documentService: DocumentService, private authenticationService: AuthenticationService) {}

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
      this.file = files.item(0);
  }

  uploadFile() {
      this.documentService.addDocument(this.file, this.authenticationService.getTokenInfo().id).subscribe(data => {
          console.log(data);
      }), error => {
          console.log(error);
      }
  }

  toggleForm():void { this.documentsComponent.toggleFalse(); }

  addDocument():void {
    // console.log(this.selectedStudyProgram);
    // if(!this.isAddOperation) {
    //   this.studentComponent.updateStudyProgram(new StudyProgram(this.selectedStudyProgram.id, this.name, this.duration, this.getCourseTypeByName(this.selectedCourseType)));
    // } else {
    //   this.studentComponent.addStudyProgram(new StudyProgram(null, this.name, this.duration, this.getCourseTypeByName(this.selectedCourseType)));
    // }
  }
}
