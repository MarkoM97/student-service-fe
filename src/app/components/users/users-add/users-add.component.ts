import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { UsersComponent } from '../users.component';
import StudyProgram from 'src/app/model/StudyProgram';
import { StudyProgramService } from 'src/app/services/study-program/study-program.service';
import { StudentsService } from 'src/app/services/students/students.service';
import { UsersService } from 'src/app/services/users/users.service';
import User from 'src/app/model/User';
import UserStudent from 'src/app/model/UserStudent';
import TeacherType from 'src/app/model/TeacherType';
import { TeacherTypeService } from 'src/app/services/teacher-type/teacher-type.service';

@Component({
  selector: 'app-users-add',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.css']
})
export class UsersAddComponent implements OnInit {
  @Input() studyPrograms: StudyProgram[];
  @Input() selectedUserr: UserStudent;
  @Input() users: User[];
  @Input() teacherTypes: TeacherType[];

  options: Boolean = true;
  isAddOperation: Boolean = true;

  studentId:string;
  firstName: string;
  lastName: string;
  address: string;
  birthday: Date;
  username: string;
  password: string;
  email: string;

  cardNumber: String;
  accountNumber: String;
  balance: Number;
  selectedStudyProgram: Number;
  modelNumber: Number;

  selectedTeacherType: number;
  selectedUser: number;

  constructor(private userComponent: UsersComponent, private studyProgramService: StudyProgramService, private studentsService: StudentsService, 
    private userService: UsersService, private teacherTypeService: TeacherTypeService) { 
    this.studyProgramService.getStudyPrograms(1, 100)
    .subscribe(
      res => {
        console.log(res);
        this.studyPrograms = res.content;
      },
      error => {
        console.log("Greska...")
      });

      this.userService.getUsersForTeacherCreate()
      .subscribe(
      res => {
        console.log(res);
        this.users = res;
        console.log(this.users);
      },
      error => {
        console.log("Greska...")
      });

      this.teacherTypeService.getTeacherTypes()
      .subscribe(
      res => {
        console.log(res);
        this.teacherTypes = res.content;
      },
      error => {
        console.log("Greska...")
      });
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.selectedUser !== undefined) {
      this.firstName = this.selectedUserr.firstname;
      this.lastName = this.selectedUserr.lastname;
      this.address = this.selectedUserr.address;
      this.birthday = this.selectedUserr.birthday;
      this.username = this.selectedUserr.username;
      this.password = this.selectedUserr.password;
      this.email = this.selectedUserr.email;

      this.cardNumber = this.selectedUserr.cardNumber;
      this.accountNumber = this.selectedUserr.accountNumber;
      this.modelNumber = this.selectedUserr.modelNumber;
      this.balance = this.selectedUserr.balance;
      this.selectedStudyProgram = this.selectedUserr.studyProgram;

      this.isAddOperation = false;
    } else {
      this.isAddOperation = true;
    }
  }

  toggleForm():void { this.userComponent.toggleFalse(); }

  getStudent(id: string) {
    return this.studentsService.getStudentById(id);
  }

  addUser():void {
    this.userService.addUser(this.firstName, this.lastName, this.address, this.birthday, this.username, this.password, this.email);
  }


  addUserStudent():void {
    console.log(this.birthday);
    this.userService.addUserStudent(this.firstName, this.lastName, this.address, this.birthday, this.username, this.password, this.email, 
      this.cardNumber, this.accountNumber, this.modelNumber, this.balance, this.selectedStudyProgram);
  }

  addUserTeacher():void {
    this.userService.addUserTeacher(this.firstName, this.lastName, this.address, this.birthday, this.username, this.password,
      this.email, this.selectedTeacherType);
  }

  editUserStudent():void {

  }

  addAll() {
    if (!this.isAddOperation) {
      if (this.options = true) {
        this.editUserStudent();
        this.toggleForm();
      } else {
        console.log("NESTO DRUGOEdit...");
      }
    } else {
      if (this.options === true) {
        this.addUserStudent();
        this.toggleForm();
        console.log("USER STUDENT");
        console.log(this.options);
      } else if (this.options === false) {
        this.addUserTeacher();
        this.toggleForm();
        console.log("USER TEACHER");
        console.log(this.options);
      }
    }
  }

}
