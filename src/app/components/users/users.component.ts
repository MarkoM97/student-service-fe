import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users/users.service';
import PaginationInfo from 'src/app/model/Pagination';
import User from 'src/app/model/User';
import UserStudent from 'src/app/model/UserStudent';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  //pagination related data
  totalPageNumber: Number[];
  page: Number = 1;
  isFirst: Boolean = true;
  isLast: Boolean = false;

  users = [];
  public isViewable: boolean;
  public paginationInfo: PaginationInfo;
  public selectedUser: UserStudent;

  constructor(private userService: UsersService) { }

  public toggle(): void { this.isViewable = true; }

  public toggleFalse(): void { this.isViewable = false; }

  ngOnInit() {
    this.updateCourseTypes(1);
    this.isViewable = false;
  }

  public updateCourseTypes(page, size = 5) {
    this.userService.getUsers(page, size).subscribe(res => {

      //pagination related data
      this.totalPageNumber = Array(res.totalPages).fill(res.totalPages).map((x,i) => i + 1);
      this.page = res.number + 1;
      this.isFirst = res.first;
      this.isLast = res.last;

      this.users = res.content;
    },err => {
      console.log(err);
    })
  }

  public paginationUpdate(number) {
    this.updateCourseTypes(number);
  } 

  editUser(user: UserStudent){
    this.selectedUser = user;
    this.toggle();
  }

  updateUser(userStudent: UserStudent) {
    this.userService.updateUserStudent(userStudent)
      .subscribe(res => {
        this.toggleFalse();
        const sp = this.users.findIndex(stpg => {
          return userStudent.id === stpg.id;
        });
        this.users[sp] = userStudent;
    }, err => {
        console.log(err);
    });
  }
  

}
