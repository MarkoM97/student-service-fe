import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users/users.service';
import User from '../../../model/User';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  user: User;

  constructor(private route: ActivatedRoute,
    private usersService: UsersService) {}

  ngOnInit() {
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      const teacherCode = +param;
      this.usersService.getUserById(param).subscribe(
        user => this.user = user,
        error => console.log("Greska...")
      );
    }
  }
}
