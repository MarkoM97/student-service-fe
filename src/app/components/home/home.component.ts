import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    const userRole = this.authenticationService.getTokenInfo().role
    if(userRole.includes("ADMINISTRATOR")) {
      this.router.navigate(['/']); 
    } else if(userRole.includes("TEACHER")) {
      this.router.navigate(['/teacher-profile']);
    } else if(userRole.includes("STUDENT")) {
      this.router.navigate(['/student-profile']);
    }

  }

  scrollToElement(id){
    event.preventDefault();
    let targetElement = document.getElementById(id);
    targetElement.scrollIntoView({behavior: "smooth"})
  }
}
