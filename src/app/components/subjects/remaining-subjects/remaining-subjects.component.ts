import { Component, OnInit } from '@angular/core';
import RemainingCourses from '../../../model/RemainingCourses';
import { SubjectsService } from '../../../services/subjects/subjects.service';
import { AuthenticationService } from '../../../services/authentication-service.service';

@Component({
  selector: 'app-remaining-subjects',
  templateUrl: './remaining-subjects.component.html',
  styleUrls: ['./remaining-subjects.component.css']
})
export class RemainingSubjectsComponent implements OnInit {

  subjects: RemainingCourses[];

  constructor(private authenticationService: AuthenticationService,
    private subjectsService: SubjectsService) { }

  ngOnInit() {
    const token = this.authenticationService.getTokenInfo();
    if(token != null){
      this.subjectsService.getRemainingCourses(token.id).subscribe(
        subjects => {this.subjects = subjects, console.log(this.subjects)},
        error => console.log("Greska...")
      );
    }
  }

}
