import { Component, OnInit, SimpleChanges } from '@angular/core';
import { AuthenticationService } from './services/authentication-service.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Student service';
  roles: string[];

  administrator = false;
  student = false;
  teacher = false;

  username: string;
  id: number;


  constructor(private authenticationService: AuthenticationService, private router: Router) {
    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    }
    )
  }

  ngOnInit() {
    const tokenInfo = this.authenticationService.getTokenInfo();
    const tokenRole = tokenInfo ? tokenInfo.role : "";
    console.log(tokenRole);
    if(tokenRole.includes("ADMINISTRATOR")) {
      this.administrator = true;
    } else if(tokenRole.includes("TEACHER")) {
      this.teacher = true;
    } else if(tokenRole.includes("STUDENT")) {
      this.student = true;
    }

   }

  logout() {
    this.authenticationService.removeToken();
    this.administrator = false;
    this.student = false;
    this.teacher = false;
    this.router.navigate(["/login"]);
  }
}