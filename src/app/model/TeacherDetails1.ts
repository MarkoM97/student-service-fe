import Subject from './Subject';

export default class TeacherDetails1 {
    id: number;
    firstname: string;
    lastname: string;
    birthday: string;
    email: string;
    address: string;
    teacherType: string;
    teacherCode: number;
    subjectsDTO: Subject[];


    constructor(id, firstname, lastname, email, address, teacherType, teacherCode){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        this.teacherType = teacherType;
        this.teacherCode = teacherCode;
        // this.subjectsDTO = subjectsDTO;
    }
  }