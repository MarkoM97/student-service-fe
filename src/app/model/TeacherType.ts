export default class TeacherType {
    id: Number;
    name: string;
    deleted: boolean;
  }