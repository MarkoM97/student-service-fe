import Exam from './Exam';

export default class ExaminationPeriod {
    id: Number;
    name: String;
    startDate: Date;
    endDate: Date;
    exams: Number[]

    constructor(id, name, startDate, endDate, exams){
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.exams = exams;
    }
  }