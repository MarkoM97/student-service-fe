import Subject from './Subject';

export default class Exam {
    id: Number;
    date: Date;
    time: String;
    classRoom: String;
    subject: Subject

    constructor(id, date, time, classRoom, subject){
        this.id = id;
        this.date = date;
        this.time = time;
        this.classRoom = classRoom;
        this.subject = subject;
    }
  }