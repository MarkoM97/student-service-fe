export default class StudentPayment {
    id: number;
    description: string;
    date: Date;
    amount: number;
    studentId: number;

    constructor(id, description, date, amount, studentId){
        this.id = id;
        this.description = description;
        this.date = date;
        this.amount = amount;
        this.studentId = studentId;
    }
}