export default class TeacherExam {
    id: number;
    date: string;
    time: string;
    classRoom: string;
    subjectName: string;
    allowInsertGrades: boolean;

    Exam(exam: TeacherExam){
        this.id = exam.id;
        this.date = exam.date;
        this.time = exam.time;
        this.classRoom = exam.classRoom;
        this.subjectName = exam.subjectName;
        this.allowInsertGrades = exam.allowInsertGrades;
    }
  }