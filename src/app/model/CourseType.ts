export default class CourseType {
    id: number;
    name: string;
    deleted: boolean;

    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.deleted = false;
    }
}