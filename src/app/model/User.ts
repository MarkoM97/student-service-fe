export default class User {
    id: number;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    birthday: string;
    email: string;
    address: string;
    deleted: boolean;

    constructor(id, username, password, firstname, lastname, birthday, email, address, deleted){
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname= firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.email = email;
        this.address = address;
        this.deleted = deleted;
    }
}