import CourseAttending from './CourseAttending';
import StudentPayment from './StudentPayment';

export default class StudentDetails {
    id: number;
    firstname: string;
    lastname: string;
    birthday: string;
    email: string;
    address: string;
    cardNumber: string;
    accountNumber: String;
    modelNumber: String;
    balance: Number;
    courseAttendingDTO: CourseAttending;
    payments: StudentPayment[];


    constructor(student: StudentDetails){
        this.id = student.id;
        this.firstname= student.firstname;
        this.lastname = student.lastname;
        this.birthday = student.birthday;
        this.email = student.email;
        this.address = student.address;
        this.cardNumber = student.cardNumber;
        this.accountNumber = student.accountNumber;
        this.balance = student.balance;
        this.modelNumber = student.modelNumber;
        this.courseAttendingDTO = student.courseAttendingDTO;
        this.payments = student.payments;
    }
  }