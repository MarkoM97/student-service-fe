export default class RemainingCourses{
    studyProgram: string;
    subjectName: string;
    teacherName: string;


    constructor(remainingCourses: RemainingCourses){
        this.studyProgram = remainingCourses.studyProgram;
        this.subjectName= remainingCourses.subjectName;
        this.teacherName = remainingCourses.teacherName;
    }
}