import Subject from './Subject';

export default class TeacherDetails {
    id: number;
    firstname: string;
    lastname: string;
    birthday: string;
    email: string;
    address: string;
    teacherType: string;
    teacherCode: number;
    subjectsDTO: Subject[];


    constructor(teacher: TeacherDetails){
        this.id = teacher.id;
        this.firstname = teacher.firstname;
        this.lastname = teacher.lastname;
        this.birthday = teacher.birthday;
        this.email = teacher.email;
        this.address = teacher.address;
        this.teacherType = teacher.teacherType;
        this.teacherCode = teacher.teacherCode;
        this.subjectsDTO = teacher.subjectsDTO;
    }
  }