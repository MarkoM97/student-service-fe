export default class UserStudent {
    id: number;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    birthday: Date;
    email: string;
    address: string;
    deleted: boolean;

    cardNumber: String;
    accountNumber: String;
    modelNumber: Number;
    balance: Number;
    studyProgram: Number;

    constructor(id, username, password, firstname, lastname, birthday, email, address, deleted, cardNumber, accountNumber, modelNumber, balance, studyProgram){
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname= firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.email = email;
        this.address = address;
        this.deleted = deleted;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.modelNumber = modelNumber;
        this.balance = balance;
        this.studyProgram = studyProgram;
    }
}