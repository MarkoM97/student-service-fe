export default class ExamPassed {
    examDate: Date;
    teacherName: string;
    points: number;
    subjectName: string;
    grade: number;

    ExamPassed(examPassed: ExamPassed){
        this.examDate = examPassed.examDate;
        this.teacherName = examPassed.teacherName;
        this.points = examPassed.points;
        this.subjectName = examPassed.subjectName;
        this.grade= examPassed.grade;
    }
}