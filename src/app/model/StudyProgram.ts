import CourseType from './CourseType';

export default class StudyProgram {
    id: Number;
    name: string;
    duration: Number;
    courseType: CourseType;
    deleted: boolean;

    constructor(id, name, duration, courseType) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.courseType = courseType;
        this.deleted = false;
    }
}