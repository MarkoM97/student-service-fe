export default class SubjectRegistrationDetails {
    examId: number;
    name: string;
    subjectCode: number;
    teacherName: string;
    date: Date;
    time: string;
    classRoom: string;
    register: string;

    ExamPassed(subjectRegistrationDetails: SubjectRegistrationDetails){
        this.examId = subjectRegistrationDetails.examId;
        this.name = subjectRegistrationDetails.name;
        this.subjectCode = subjectRegistrationDetails.subjectCode;
        this.teacherName = subjectRegistrationDetails.teacherName;
        this.date = subjectRegistrationDetails.date;
        this.time = subjectRegistrationDetails.time;
        this.classRoom = subjectRegistrationDetails.classRoom;
        this.register = subjectRegistrationDetails.register;
    }
}