import TeacherType from './TeacherType';

export default class Teacher {
    teacherCode: number;
    deleted: boolean;
    teacherType: TeacherType;

    constructor(teacherCode: number, deleted: boolean, teacherType: TeacherType){
      this.teacherCode = teacherCode;
      this.deleted = deleted;
      this.teacherType = teacherType;
    }
  }