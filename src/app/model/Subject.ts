
export default class Subject {
    id: number;
    name: string;
    subjectCode: number;

    constructor(id, name, subjectCode){
        this.id = id;
        this.name = name;
        this.subjectCode = subjectCode;
    }
  }