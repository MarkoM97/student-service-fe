//Represents JSON structure of paginated data coming from BE
export default class PaginationInfo {
    content: [];
    first: Boolean;
    last: Boolean;
    number: number;
    numberOfElements: Number;
    pageable: Object;
    size: Number;
    sort: Object;
    totalElements: Number;
    totalPages: Number;
}