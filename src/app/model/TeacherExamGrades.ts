export default class TeacherExamGrades {
    examId: number;
    subjectName: string;
    studentName: string;
    studentId: number;
    grade: number;
    points: number;

    ExamPassed(teacherExamGrades: TeacherExamGrades){
        this.examId = teacherExamGrades.examId;
        this.subjectName = teacherExamGrades.subjectName;
        this.studentName = teacherExamGrades.studentName;
        this.studentId = teacherExamGrades.studentId;
        this.grade = teacherExamGrades.grade;
        this.points = teacherExamGrades.points;
    }
}