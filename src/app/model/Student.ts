export default class Student {
    cardNumber: String;
    accountNumber: String;
    balance: Number;
    studyProgram: Number;
  }