export default class CourseAttending {
    id: number;
    dateOfEnrollment: Date;
    studyProgramId: string;
    studyProgramName: string;
    studentId: string;
    deleted: boolean;


    constructor(courseAttending: CourseAttending){
        this.id = courseAttending.id;
        this.dateOfEnrollment= courseAttending.dateOfEnrollment;
        this.studyProgramId = courseAttending.studyProgramId;
        this.studyProgramName = courseAttending.studyProgramName;
        this.studentId = courseAttending.studentId;
        this.deleted = courseAttending.deleted;
    }
  }