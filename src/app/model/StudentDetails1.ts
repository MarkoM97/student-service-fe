import CourseAttending from './CourseAttending';

export default class StudentDetails1 {
    id: string;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    birthday: Date;
    email: string;
    address: string;
    cardNumber: string;
    accountNumber: String;
    modelNumber: String;
    balance: Number;
    courseAttendingDTO: CourseAttending;
    studyProgram: string    ;


    constructor(id, username, password, birthday, firstname, lastname, email, address, cardNumber, accountNumber, modelNumber, balance, courseAttendingDTO, studyProgram){
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname= firstname;
        this.lastname = lastname;
        this.birthday = birthday;
        this.email = email;
        this.address = address;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.modelNumber = modelNumber;
        this.courseAttendingDTO = courseAttendingDTO;
        this.studyProgram = studyProgram;
    }
  }