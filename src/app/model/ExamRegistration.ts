import SubjectRegistrationDetails from './SubjectRegistrationDetails';
export default class ExamRegistration {
    examinationPeriod: string;
    subjects: SubjectRegistrationDetails[];
    studentBalance: number;

    ExamPassed(examRegistration: ExamRegistration){
        this.examinationPeriod = examRegistration.examinationPeriod;
        this.subjects = examRegistration.subjects;
        this.studentBalance = examRegistration.studentBalance;
    }
}