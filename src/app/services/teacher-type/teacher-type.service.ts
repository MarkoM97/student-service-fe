import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeacherTypeService {
  constructor(private httpClient: HttpClient) { }

  getTeacherTypes() {
      return this.httpClient.get("/api/teacher-type") as Observable<any>;
  }

  addTeacherTypes(name, deleted) {
    const obj = {
      name: name,
      deleted: deleted
    };
    this.httpClient.post("/api/teacher-type", obj)
        .subscribe(res => console.log('Done'));
  }
}
