import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import User from '../../model/User';
import UserStudent from 'src/app/model/UserStudent';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient) { }

  // getUsers() {
  //   return this.httpClient.get("/api/users") as Observable<any>;
  // }

  getUsers(page: Number, size: Number) {
    return this.httpClient.get(`/api/users?page=${page}&size=${size}`) as Observable<any>;
  }

  getAdmins(page: Number, size: Number) {
    return this.httpClient.get(`/api/users/administrators?page=${page}&size=${size}`) as Observable<any>;
  }

  getUserById(id: string) {
    return this.httpClient.get<User>("/api/users/"+ id);
  }

  getUsersForTeacherCreate(){
    return this.httpClient.get("/api/users/without-teacher-student") as Observable<any>;
  }

  addUser(firstname, lastname, address, birthday, username, password, email) {
    const obj = {
      firstname: firstname,
      lastname: lastname,
      address: address,
      birthday: birthday,
      username: username,
      password: password,
      email: email
    };
    this.httpClient.post("/api/users/create-admin", obj)
        .subscribe(res => console.log('Done'));
  }

  addUserStudent(firstname, lastname, address, birthday, username, password, email, 
    cardNumber, accountNumber, modelNumber, balance, studyProgram) {
      console.log("usao ovde");
    const obj = {
      firstname: firstname,
      lastname: lastname,
      address: address,
      birthday: birthday,
      username: username,
      password: password,
      email: email,
      cardNumber: cardNumber,
      accountNumber: accountNumber,
      modelNumber: modelNumber,
      balance: balance,
      studyProgram: studyProgram
    };
    this.httpClient.post("/api/users/create", obj)
        .subscribe(res => console.log('Done'));
  }

  addUserTeacher(firstname, lastname, address, birthday, username, password, email, teacherTypeId) {
    const obj = {
      firstname: firstname,
      lastname: lastname,
      address: address,
      birthday: birthday,
      username: username,
      password: password,
      email: email,
      teacherTypeId: teacherTypeId
    };
    this.httpClient.post("/api/users/create-teacher", obj)
        .subscribe(res => console.log('Done'));
  }

  updateUserStudent(userStudent: UserStudent) {
    return this.httpClient.put("/api/users/" + userStudent.id, userStudent) as Observable<any>;
  }

  deleteUser(id){
    return this.httpClient.delete("/api/users/"+ id) as Observable<any>;
  }

  updateAdmin(admin: User) {
    return this.httpClient.put(`/api/users/${admin.id}`, admin) as Observable<any>;
  }

}