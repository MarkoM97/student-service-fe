import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import TeacherExamGrades from '../../model/TeacherExamGrades';

@Injectable({
  providedIn: 'root'
})
export class ExamGradesService {

  constructor(private httpClient: HttpClient) { }

  getTeacherExamGrades(examId: string){
    return this.httpClient.get<TeacherExamGrades[]>("/api/exam-grades/teacher/exam/"+ examId);
  }

  createExamGrades(examGrades: TeacherExamGrades[]){
    this.httpClient.post("/api/exam-grades", examGrades)
        .subscribe(res => console.log('Done'));
  }
}
