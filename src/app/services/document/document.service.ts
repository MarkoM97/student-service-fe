import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Exam from '../../model/Exam';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private httpClient: HttpClient) { }

//   getDocuments(page: Number, size: Number) {
//     return this.httpClient.get(`/api/documents?studentId=${page}&size=${size}`) as Observable<any>;
//   }

    getDocuments(studentId: Number) {
        return this.httpClient.get(`/api/documents?studentId=${studentId}`) as Observable<any>;
    }

    startDownload(id: Number) {
        return this.httpClient.get(`http://localhost:8080/api/documents/download/${id}`) as Observable<any>;
    }

  addDocument(fileToUpload: File, studentId: string) {
    const endpoint = `http://localhost:8080/api/documents/upload`;
    let formData: FormData = new FormData();
    formData.append("file", fileToUpload, fileToUpload.name);
    formData.append("studentId", studentId);
    return this.httpClient.post(endpoint, formData) as Observable<any>;
  }

  deleteDocument(id: Number) {
    return this.httpClient.delete("http://localhost:8080/api/documents/" + id) as Observable<any>;
  }
}
