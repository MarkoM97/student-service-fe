import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Exam from '../../model/Exam';

@Injectable({
  providedIn: 'root'
})
export class ExamService {

  constructor(private httpClient: HttpClient) { }

  getExams(page: Number, size: Number) {
    return this.httpClient.get(`/api/exams?page=${page}&size=${size}`) as Observable<any>;
  }

  getDetailedExams(page: Number, size: Number){
    return this.httpClient.get(`/api/exams/detailed?page=${page}&size=${size}`) as Observable<any>;
  }

  getAllDetailedExams(){
    return this.httpClient.get(`/api/exams/all-detailed`) as Observable<any>;
  }

  addExam(obj: Exam) {
    return this.httpClient.post("/api/exams", obj) as Observable<any>;
  }

  update(obj: Exam) {
    return this.httpClient.put("/api/exams/" + obj.id,obj) as Observable<any>;
  }

  delete(id: Number) {
    return this.httpClient.delete("/api/exams/" + id) as Observable<any>;
  }
}
