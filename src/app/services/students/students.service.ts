import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import StudentDetails from '../../model/StudentDetails';
import UserStudent from 'src/app/model/UserStudent';
import StudentDetails1 from 'src/app/model/StudentDetails1';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(private httpClient: HttpClient) { }

  getStudents(page: Number, size: Number) {
      return this.httpClient.get(`/api/students?page=${page}&size=${size}`) as Observable<any>;
  }

  getStudentById(id: string) {
    return this.httpClient.get<StudentDetails>("/api/students/user/"+ id);
  }
  getStudentByUsername(username: string) {
    return this.httpClient.get<StudentDetails>("/api/students/username/"+ username);
  }

  addStudent(student: StudentDetails1) {
    return this.httpClient.post("/api/students", student) as Observable<any>;
  }

  updateUser(userStudent: StudentDetails1) {
    return this.httpClient.put("/api/students/" + userStudent.id, userStudent) as Observable<any>;
  }

  deleteStudent(id: number) {
    this.httpClient.delete("/api/students/" + id)
      .subscribe(res => console.log('Done'));
  }

}