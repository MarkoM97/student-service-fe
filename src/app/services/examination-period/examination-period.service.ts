import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import ExaminationPeriod from '../../model/ExaminationPeriod';


@Injectable({
  providedIn: 'root'
})
export class ExaminationPeriodService {

  constructor(private httpClient: HttpClient) { }

  getExamPeriods(page: Number, size: Number) {
    return this.httpClient.get(`/api/exam-periods?page=${page}&size=${size}`) as Observable<any>;
  }

  addExamPeriod(exPeriod: ExaminationPeriod) {
    return this.httpClient.post("/api/exam-periods", exPeriod) as Observable<any>;
  }

  update(eP: ExaminationPeriod){
    return this.httpClient.put("/api/exam-periods/"+eP.id, eP) as Observable<any>;
  }

  delete(id: Number) {
    return this.httpClient.delete("/api/exam-periods/" + id)as Observable<any>;  
  }
}
