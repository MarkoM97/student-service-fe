import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtUtilsService } from './jwt-utils-service.service';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';


@Injectable()
export class AuthenticationService {

  private readonly loginPath = "/api/users/login";

  constructor(private http: HttpClient, private jwtUtilsService: JwtUtilsService) { }

  login(username: string, password: string): Observable<boolean> {
    var headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.loginPath, JSON.stringify({ username, password }), { headers }).pipe(
      map((res: any) => {
        let token = res && res['token'];
        if (token) {
          localStorage.setItem("accessToken", token);
          return true;
        }
        else {
          return false;
        }
      }),
      catchError((error: any) => {
        if (error.status === 400) {
          return throwError('Ilegal login');
        }
        else {
          return throwError(error.json().error || 'Server error');
        }
      }));
  }

  getToken(): String {
    return localStorage.getItem("accessToken") || null;
  }

  removeToken() {
    localStorage.removeItem('accessToken');
  }

  isLoggedIn(): boolean {
    if (this.getToken() != '') return true;
    else return false;
  }

  getTokenInfo() {
    const tokenVal = this.getToken();
    if (tokenVal) {
      return jwt_decode(tokenVal);
    }
    return null;
  }

}
