import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import StudentPayment from '../../model/StudentPayment';

@Injectable({
  providedIn: 'root'
})
export class StudentPaymentsService {

  constructor(private httpClient: HttpClient) { }

  getUserPayments(param: String,page: Number, size: Number) {
    return this.httpClient.get(`/api/payments/student/${param}?page=${page}&size=${size}`) as Observable<any>;
  }

  addPayment(payment: StudentPayment){
    this.httpClient.post("/api/payments", payment)
        .subscribe(res => console.log('Done'));
  }
}
