import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import CourseType from 'src/app/model/CourseType';

@Injectable({
  providedIn: 'root'
})
export class CourseTypesService {

  constructor(private httpClient: HttpClient) { }

  getCourseTypes(page: Number, size: Number) {
    return this.httpClient.get(`/api/course-types?page=${page}&size=${size}`) as Observable<any>;
  }

  addCourseType(name) {
    const obj = {
      name: name
    };
    this.httpClient.post("/api/course-types", obj)
        .subscribe(res => console.log('Done'));
  }

  updateCourseType(courseType: CourseType) {
    return this.httpClient.put(`/api/course-types/${courseType.id}`, courseType) as Observable<any>;
  }

  deleteCourseType(id: Number) {
    return this.httpClient.delete(`/api/course-types/${id}`) as Observable<any>;
  }

}
