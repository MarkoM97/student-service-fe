import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import StudyProgram from 'src/app/model/StudyProgram';

@Injectable({
  providedIn: 'root'
})
export class StudyProgramService {

  constructor(private httpClient: HttpClient) { }

  getStudyPrograms(page: Number, size: Number) {
    return this.httpClient.get(`/api/study-programs?page=${page}&size=${size}`) as Observable<any>;
  }

  addStudyProgram(studyProgram: StudyProgram) {
    return this.httpClient.post(`/api/study-programs/`, studyProgram) as Observable<any>;
  }

  updateStudyProgram(studyProgram: StudyProgram) {
    return this.httpClient.put(`/api/study-programs/${studyProgram.id}`, studyProgram) as Observable<any>;
  }

  deleteStudyProgram(id: Number) {
    return this.httpClient.delete(`/api/study-programs/${id}`) as Observable<any>;
  }
}