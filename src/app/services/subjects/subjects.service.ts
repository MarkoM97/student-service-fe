import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import RemainingCourses from '../../model/RemainingCourses';
import Subject from 'src/app/model/Subject';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  constructor(private httpClient: HttpClient) { }
  
  getAllCourses(){
    return this.httpClient.get("/api/subjects/all") as Observable<any>;
  }

  getRemainingCourses(id: number) {
      return this.httpClient.get<RemainingCourses[]>('/api/subjects/remaining-courses/'+ id) ;
  }
}
