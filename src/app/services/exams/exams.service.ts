import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import ExamPassed from '../../model/ExamPassed';
import ExamRegistration from '../../model/ExamRegistration';
import TeacherExam from '../../model/TeacherExam';

@Injectable({
  providedIn: 'root'
})
export class ExamsService {

  constructor(private httpClient: HttpClient) { }

  getExams(page: Number, size: Number) {
    return this.httpClient.get(`/api/exams?page=${page}&size=${size}`) as Observable<any>;
  }

  getAllExamsForStudent(id: string){
    return this.httpClient.get<ExamPassed[]>("api/exams/user/" + id);
  }

  getDetailedExams(){
    return this.httpClient.get(`/api/exams/detailed`) as Observable<any>;
  }

  getExamPassedByStudent(id: string) {
    return this.httpClient.get<ExamPassed[]>("/api/exam-grades/user/"+ id);
  }

  getRegisteredExams(studentId: string){
    
    return this.httpClient.get<ExamRegistration>("/api/exams/registered-exams/user/"+ studentId);
  }

  getCurrentExamsForTeacher(teacherId: string){
    
    return this.httpClient.get<TeacherExam[]>("/api/exams/current-exams/teacher/"+ teacherId);
  }

  getAllExamsForTeacher(teacherId: string){
    
    return this.httpClient.get<TeacherExam[]>("/api/exams/teacher/"+ teacherId);
  }
  
  getCurrentPeriod(id: number) {
    return this.httpClient.get<ExamRegistration[]>("/api/exam-periods/current-period/student/"+ id);
  }
  registerExam(examId: number, studentId: number) {
    return this.httpClient.post("/api/exam-grades/register-exam", {examId: examId, studentId: studentId}) as Observable<any>;
  }
}
