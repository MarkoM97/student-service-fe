import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Teacher from '../../model/Teacher';
import { map, retry, catchError } from 'rxjs/operators';
import TeacherDetails from '../../model/TeacherDetails';
import TeacherDetails1 from 'src/app/model/TeacherDetails1';

@Injectable({
  providedIn: 'root'
})
export class TeachersService {
	private teacherUrl = 'api/teachers';

  constructor(private httpClient: HttpClient) { }

  getTeachers(page: Number, size: Number) {
    return this.httpClient.get(`/api/teachers?page=${page}&size=${size}`) as Observable<any>;
  }

  getTeacher(teacherCode: number){
    return this.httpClient.get<TeacherDetails>("api/teachers/" + teacherCode)
  }

  deleteTeacher(id: number){
    this.httpClient.delete("api/teachers/"+ id)
      .subscribe(res => console.log('Done'));
  }

  addTeacher(userId, teacherTypeId) {
    const obj = {
      userId: userId,
      teacherTypeId: teacherTypeId
    };
    this.httpClient.post("/api/teachers", obj)
        .subscribe(res => console.log('Done'));
  }

  updateTeacher(teacher: TeacherDetails1) {
    return this.httpClient.put("/api/teachers/" + teacher.id, teacher) as Observable<any>;
  }

}
