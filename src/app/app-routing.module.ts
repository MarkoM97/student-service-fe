import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StudentsComponent } from './components/students/students.component';
import { LoginComponent } from './components/login/login.component';
import { TeacherComponent } from './components/teachers/teacher.component';
import { TeacherDetailsComponent } from './components/teachers/teacher-details/teacher-details.component';
import { UsersComponent } from './components/users/users.component';
import { StudentDetailsComponent } from './components/students/student-details/student-details.component';
import { RemainingSubjectsComponent } from './components/subjects/remaining-subjects/remaining-subjects.component';
import { HomeComponent } from './components/home/home.component';
import { ExaminationPeriodComponent } from  './components/exams/examination-period/examination-period.component';
import { ExamComponent } from './components/exams/exam/exam.component';
import { StudentRegisteredExamsComponent } from './components/students/exams/registered-exams/student-registered-exams.component';
import { StudentPassedExamsComponent } from './components/students/exams/passed-exams/student-passed-exams.component';
import { StudentExamRegistrationComponent } from './components/students/exams/exam-registration/student-exam-registration.component';
import { TeachersExamGradesComponent } from './components/teachers/exams/exam-grades/teachers-exam-grades.component';
import { StudyProgramsComponent } from './components/study-programs/study-programs.component'
import { CourseTypesComponent } from './components/course-types/course-types.component';
import { TeacherCurrentExamsComponent } from './components/teachers/exams/teacher-current-exams/teacher-current-exams.component';
import { AuthGuard } from './guards/auth.guard';
import { StudentPaymentsComponent } from './components/students/student-payments/student-payments.component';
import { TeacherAllExamsComponent } from './components/teachers/exams/all-exams/teacher-all-exams.component';
import { AdminsComponent } from './components/admins/admins.component';
import { AdminDetailsComponent } from './components/admins/admin-details/admin-details.component';
import { StudentExamHistoryComponent } from './components/students/exams/exam-history/student-exam-history.component';
import { DocumentsComponent } from './components/students/documents/documents.component';

const routes: Routes = [
  { path: '', component: HomeComponent , pathMatch: 'full', canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR", "TEACHER", "STUDENT"]} },
  { path: 'exam-period', component: ExaminationPeriodComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'exams', component: ExamComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'login', component: LoginComponent },
  { path: 'administrator-profile', component: AdminDetailsComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'students', component: StudentsComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'teachers', component: TeacherComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'administrators', component: AdminsComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  { path: 'study-programs', component: StudyProgramsComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]}},
  { path: 'course-types', component: CourseTypesComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]}},
  { path: 'student-payments/:id', component: StudentPaymentsComponent, canActivate: [AuthGuard], data: {roles : ["ADMINISTRATOR"]} },
  
  { path: 'student-profile', component: StudentDetailsComponent, canActivate: [AuthGuard], data: {roles : ["STUDENT"]} },
  { path: 'passed-exams', component: StudentPassedExamsComponent, canActivate: [AuthGuard], data: {roles : ["STUDENT"]} },
  { path: 'remaining-courses', component: RemainingSubjectsComponent, canActivate: [AuthGuard], data: {roles : ["STUDENT"]} },
  { path: 'student-exam-registration', component: StudentExamRegistrationComponent, canActivate: [AuthGuard], data: {roles : ["STUDENT"]} },
  { path: 'student-registered-exams', component: StudentRegisteredExamsComponent, canActivate: [AuthGuard], data: {roles : ["STUDENT"]} },
  { path: 'student-exam-history', component: StudentExamHistoryComponent, canActivate: [AuthGuard], data: {roles : ["STUDENT"]} },
  { path: 'student-documents', component: DocumentsComponent, canActivate: [AuthGuard], data: {roles: ["STUDENT"]}},
  
  { path: 'teacher-profile', component: TeacherDetailsComponent, canActivate: [AuthGuard], data: {roles : ["TEACHER"]} },
  { path: 'teacher-exams', component: TeacherAllExamsComponent, canActivate: [AuthGuard], data: {roles : ["TEACHER"]} },
  { path: 'teacher-exam-grades/:id', component: TeachersExamGradesComponent, canActivate: [AuthGuard], data: {roles : ["TEACHER"]} },
  { path: 'teacher-current-exams', component: TeacherCurrentExamsComponent, canActivate: [AuthGuard], data: {roles : ["TEACHER"]} },
  
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }