import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';

import { AuthenticationService } from '../services/authentication-service.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authenticationService: AuthenticationService, private location: Location) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const tokenUser = this.authenticationService.getTokenInfo();
        if(tokenUser) {
            if (!route.data.roles.includes(tokenUser.role)) {
                this.location.back();
                return false;
            }
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}